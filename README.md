# Parcel

- [ ] Consult [albizures/parcel-rust](https://github.com/albizures/parcel-rust) for more information and inspiration
- [ ] Figure out why commenting out the Rust `add` function call works but not commenting it out results in an empty page with no errors
- [ ] Add an example of straight up `import`ing a WebAssembly file
- [ ] Add an example of `import`ing Rust more complexly by importing a Cargo crate and not a simple Rust file

## Installing

`yarn global add parcel-bundler`

## Running

`parcel index.html`

## Integrating

### TypeScript

It just works! Reference `index.ts` in your `index.html` and you're all set bruv.

### Rust



## Contributing

- [ ] Try out [Rust in Parcel](https://www.reddit.com/r/rust/comments/7z8imh/announcing_stdweb_04_now_with_support_for_parcel/)
