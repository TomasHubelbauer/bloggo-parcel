import * as qrcode from 'qrcode-generator';
import { add } from './test.rs';

window.addEventListener('load', async _ => {
  const qr = qrcode(0, 'H');
  qr.addData(await parcel());
  qr.make();
  document.body.innerHTML = qr.createImgTag(10);
  console.log(add);
});


async function parcel() {
  return 'Parcel';
}
